/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control;

import android.view.MotionEvent;

import org.sasehash.burgerwp.Control.Actions.FleeAction;

public class ActionFactory {
    public static Action create(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            float x = event.getX();
            float y = event.getY();
            //return new VacuumAction(x, y);
            return new FleeAction(x, y);
        }
      //if (event.getAction() == MotionEvent.ACTION_DOWN) {
      //    float x = event.getX();
      //    float y = event.getY();
      //    return new VacuumAction(x, y);
      //}
        return null;
    }
}
