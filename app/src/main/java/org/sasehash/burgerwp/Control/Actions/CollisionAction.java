/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.Actions;

import org.sasehash.burgerwp.Control.Action;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

/**
 * Collision against some wall
 */
public class CollisionAction implements Action {
    @Override
    public void update(WPState wpState) {
        entity.state.collide(direction);
    }

    Entity entity;
    Direction direction;


    public CollisionAction(Direction d, Entity e) {
        entity = e;
        direction =d;
    }

    public enum Direction {N, E, S, W}
}
