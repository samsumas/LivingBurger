/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control;

import org.sasehash.burgerwp.Control.Actions.CollisionAction;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

/**
 * Used to make Entities move. An Entity can be running to some Coordinates or doing something funnier.
 */
public interface EntityState {
    void advance(long deltaTime, Entity self, WPState wpState);

    void collide(CollisionAction.Direction d);
}
