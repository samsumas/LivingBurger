/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.EntityStates;

import org.sasehash.burgerwp.Control.Actions.CollisionAction;
import org.sasehash.burgerwp.Control.EntityState;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

/**
 * As the name says ... this class does nothing!
 */
public class ProcrastinatingState implements EntityState {
    @Override
    public void advance(long deltaTime, Entity self, WPState wpState) {}

    public void collide(CollisionAction.Direction d) {
    }
}
