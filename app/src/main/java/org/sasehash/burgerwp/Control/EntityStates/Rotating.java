/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.EntityStates;

import org.sasehash.burgerwp.Control.Actions.CollisionAction;
import org.sasehash.burgerwp.Control.EntityState;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

public class Rotating implements EntityState {
    private float r;

    /**
     * Makes entities rotate around their own axis with r in degrees per s
     *
     * @param r : rotation speed
     */
    public Rotating(float r) {
        this.r = r / 1000;
    }

    @Override
    public void advance(long deltaTime, Entity self, WPState wpState) {
        self.rotation += r * deltaTime;
    }

    @Override
    public void collide(CollisionAction.Direction d) {
        //Oh No!
        r = -r;
    }
}
