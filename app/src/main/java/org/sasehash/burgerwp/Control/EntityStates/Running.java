/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.EntityStates;

import org.sasehash.burgerwp.Control.Actions.CollisionAction;
import org.sasehash.burgerwp.Control.EntityState;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

public class Running implements EntityState {
    private float dirX, dirY;
    private float speed;

    public Running(float dirX, float dirY, float speed) {
        float length = (float) Math.sqrt(Math.pow(dirX, 2) + Math.pow(dirY, 2));
        this.dirX = dirX / length;
        this.dirY = dirY / length;
        this.speed = speed;
    }

    @Override
    public void collide(CollisionAction.Direction d) {
        switch (d) {
            case N:
                dirY = Math.abs(dirY);
                break;
            case E:
                dirX = -Math.abs(dirX);
                break;
            case S:
                dirY = -Math.abs(dirY);
                break;
            case W:
                dirX = Math.abs(dirX);
                break;
        }
    }

    @Override
    public void advance(long deltaTime, Entity self, WPState wpState) {
        self.x += dirX * speed * ((float) deltaTime) / 1000;
        self.y += dirY * speed * ((float) deltaTime) / 1000;
    }
}

