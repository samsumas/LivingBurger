/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Control.EntityStates;

import org.sasehash.burgerwp.Control.Actions.CollisionAction;
import org.sasehash.burgerwp.Control.EntityState;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

public class RunningInCircles implements EntityState {
    private float x, y, rotation, radius, speed;

    public RunningInCircles(float x, float y, float rotation, float radius, float speed) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.radius = radius;
        this.speed = speed;
    }

    @Override
    public void advance(long deltaTime, Entity self, WPState wpState) {
        rotation += speed * ((float)deltaTime) /1000;
        self.x = x + (float) Math.sin(rotation) * radius;
        self.y = y + (float) Math.cos(rotation) * radius;
    }

    @Override
    public void collide(CollisionAction.Direction d) {
        //TODO: is this working?
        speed = -speed;
    }
}
