/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import org.sasehash.burgerwp.Control.Action;
import org.sasehash.burgerwp.Control.ActionFactory;
import org.sasehash.burgerwp.Control.Actions.CollisionAction;
import org.sasehash.burgerwp.Model.ConcurrentActions;
import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;
import org.sasehash.burgerwp.View.WPView;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JumpingBurger extends WallpaperService {

    @Override
    public Engine onCreateEngine() {
        return new JumpingEngine();
    }

    private class JumpingEngine extends Engine {
        /* Values that can be tweaked */
        private final static int SLEEP_BETWEEN_TWO_FRAMES = 35;
        private Handler handler = new Handler();
        private boolean visibility = true;
        private Paint p = new Paint();
        private int width, height;
        private Bitmap backgroundImage;
        private boolean useBackgroundImage;

        private WPState wpState;
        private WPView wpView;
        private ConcurrentActions actions = new ConcurrentActions();
        private long lastDraw = -1;
        private Context context;

        private final Runnable drawRunner = new Runnable() {
            @Override
            public void run() {
                draw(false);
            }
        };

        private final Runnable startDrawRunner = new Runnable() {
            @Override
            public void run() {
                draw(true);
            }
        };


        public JumpingEngine() {
            /* Load values from preferences */
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(JumpingBurger.this);
            useBackgroundImage = settings.getBoolean("pref_bg_color_or_bg_image", false);

            context = getApplicationContext();

            if (useBackgroundImage) {
                try {
                    String filename = settings.getString("pref_bg_image", null);
                    if (filename == null) {
                        throw new IllegalStateException("Failed to get ImageName with intent");
                    }
                    if (filename.equals("abc")) {
                        throw new IllegalStateException("Got Standard (invalid) filename");
                    }
                    Uri uri = Uri.parse(filename);
                    try {
                        InputStream is = getContentResolver().openInputStream(uri);
                        backgroundImage = BitmapFactory.decodeStream(is);
                        //close inputstream
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                //well don't care about it, can't close what isn't opened lol
                            }
                        }
                    } catch (java.lang.SecurityException e) {
                        //FIXME

                        stopUsingBackgroundImage();
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //continue with color instead of background
                    stopUsingBackgroundImage();
                }
            }
            p.setFilterBitmap(false);
            p.setColor(Color.BLACK);
            p.setStyle(Paint.Style.FILL);
        }

        /**
         * stopUsingBackgroundImage
         */
        private void stopUsingBackgroundImage() {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(JumpingBurger.this).edit();
            editor.putBoolean("pref_bg_color_or_bg_image", false);
            editor.apply();
            useBackgroundImage = false;
        }

        private Bitmap loadImage(SharedPreferences settings, String s) {
            Bitmap texture;
            try {
                //load externalResource
                if (Boolean.parseBoolean(settings.getString(s + "_isExternalResource", "false"))) {
                    texture = BitmapFactory.decodeFile(settings.getString(s + "_image", ""));
                } else {
                    String id = settings.getString(s + "_image", null);
                    texture = BitmapFactory.decodeResource(getResources(), Integer.parseInt(id));
                }
            } catch (Exception e) {
                //when failling to load texture, use the burger one
                //FIXME
                texture = BitmapFactory.decodeResource(getResources(), R.drawable.burger);
                e.printStackTrace();
            }
            return texture;
        }

        /**
         * loads an image from a URI, copy pasted from api doc
         *
         * @param uri the uri of image
         * @return image, the image as bitmap
         * @throws IOException when something failed
         */
        private Bitmap getBitmapFromUri(Uri uri) throws IOException {
            ParcelFileDescriptor parcelFileDescriptor =
                    getContentResolver().openFileDescriptor(uri, "r");
            if (parcelFileDescriptor == null) {
                throw new IOException("FileDescriptor broken!");
            }
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;
        }

        /**
         * checkValue, throws an error if value is not set
         *
         * @param a        key of value
         * @param settings the actual settings
         */
        private void checkValue(String a, SharedPreferences settings) {
            if (!settings.contains(a)) {
                throw new IllegalStateException("Cannot read " + a + " Settings from anim_preferences!");
            }
        }

        /**
         * Draw background and tile it if too small
         *
         * @param bmp    : background
         * @param canvas : where to draw the background
         */
        private void tilingAndDraw(Bitmap bmp, Canvas canvas) {
            int x = 0, y = 0;
            //we use tiling for background pictures that are too small
            while (x < width && y < height) {
                canvas.drawBitmap(backgroundImage, x, y, p);
                //draw a column
                if (y + backgroundImage.getHeight() < height) {
                    y += backgroundImage.getHeight();
                    continue;
                }
                //next column
                y = 0;
                if (x + backgroundImage.getWidth() < width) {
                    x += backgroundImage.getWidth();
                    continue;
                }
                //nothing to do anymore
                break;
            }
        }

        /**
         * Compute the time passed (deltaT) between now and the last draw
         *
         * @return deltaT
         */
        private long deltaT() {
            long t = System.currentTimeMillis();
            long deltaT;
            if (lastDraw == -1) {
                deltaT = 0;
            } else {
                deltaT = t - lastDraw;
            }
            lastDraw = t;
            return deltaT;
        }

        /**
         * Simulate one step that is deltaT ms long.
         *
         * @param deltaT Time elapsed in the simulation.
         */
        private void simulate(long deltaT) {
            for (Action a : actions.clear()) {
                a.update(wpState);
            }
            for (Action a : computeCollisions()) {
                a.update(wpState);
            }
            for (Entity e : wpState.entities) {
                e.advance(deltaT, wpState);
            }
        }

        private List<Action> computeCollisions() {
            ArrayList<Action> ret = new ArrayList<>();
            for (Entity e : wpState.entities) {
                //left
                if (e.x <= 0) {
                    ret.add(new CollisionAction(CollisionAction.Direction.W, e));
                }
                if (e.y <= 0) {
                    ret.add(new CollisionAction(CollisionAction.Direction.N, e));
                }
                if (e.x + e.width >= wpState.screenSizeX - 1) {
                    ret.add(new CollisionAction(CollisionAction.Direction.E, e));
                }
                if (e.y + e.height >= wpState.screenSizeY - 1) {
                    ret.add(new CollisionAction(CollisionAction.Direction.S, e));
                }
            }
            return ret;
        }

        /**
         * Simulate one step and draw. This happens in the main loop of the engine.
         */
        private void draw(boolean started) {
            long deltaT = deltaT();
            SurfaceHolder holder = getSurfaceHolder();
            Canvas canvas = null;

            if (!started) {
                simulate(deltaT);
            }

            try {
                canvas = holder.lockCanvas();
                if (useBackgroundImage) {
                    tilingAndDraw(backgroundImage, canvas);
                } else {
                    canvas.drawColor(wpState.bg_color);
                }

                wpView.draw(canvas);
            } finally {
                if (canvas != null) {
                    holder.unlockCanvasAndPost(canvas);
                }
            }
            handler.removeCallbacks(drawRunner);
            if (visibility) {
                handler.postDelayed(drawRunner, SLEEP_BETWEEN_TWO_FRAMES);
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            this.visibility = visible;
            if (visible) {
                handler.post(startDrawRunner);
            } else {
                handler.removeCallbacks(drawRunner);
            }
        }

        @Override
        public void onTouchEvent(MotionEvent event) {
            Action a = ActionFactory.create(event);
            if (a != null) {
                actions.add(a);
            }
        }


        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            this.visibility = false;
            handler.removeCallbacks(drawRunner);
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format,
                                     int width, int height) {
            wpState = new WPState(PreferenceManager.getDefaultSharedPreferences(JumpingBurger.this), getResources(), width, height);
            wpView = new WPView(wpState, p);
            //load all objects from config
            this.width = width;
            this.height = height;
            wpState.screenSizeX = width;
            wpState.screenSizeY = height;
            super.onSurfaceChanged(holder, format, width, height);
        }
    }


}
