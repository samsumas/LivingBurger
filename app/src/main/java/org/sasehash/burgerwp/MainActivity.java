/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp;

import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //preview the wallpaper and the possibility to set it as current wallpaper
    public void startPreview(View view) {
        Intent intent = new Intent(
                WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
        intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,
                new ComponentName(this, JumpingBurger.class));
        startActivity(intent);
    }
    //start settings activity
    public void startSettings(View view) {
        startConfigurator(view);
    }

    public void startConfigurator(View view) {
        startActivity(new Intent(this, Configurator.class));
    }
}
