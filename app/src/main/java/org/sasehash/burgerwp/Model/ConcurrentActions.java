/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Model;

import org.sasehash.burgerwp.Control.Action;

import java.util.ArrayList;
import java.util.List;

public class ConcurrentActions {
    private ArrayList<Action> l = new ArrayList<>();

    public synchronized void add(Action a) {
        l.add(a);
    }

    public synchronized List<Action> clear() {
        List<Action> ret = l;
        l = new ArrayList<>();
        return ret;
    }

}
