/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.Model;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.sasehash.burgerwp.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class WPState {
    public ArrayList<Entity> entities;

    public int screenSizeX;
    public int screenSizeY;

    public int bg_color;

    Entity grabbed;

    Resources resources;

    public WPState(SharedPreferences settings, Resources resources, int width, int height) {
        this.resources = resources;
        entities = new ArrayList<>();
        Set<String> objectNames = settings.getStringSet("objects", null);


        //reset settings on error
        if (objectNames == null) {
            reset(settings, resources);
            objectNames = settings.getStringSet("objects", new HashSet<String>());
        }

        bg_color = settings.getInt("bg_color_int", 0xFF000000);

        int idx = 0;
        screenSizeY = height;
        screenSizeX = width;
        Random r = new Random();
        for (String s : objectNames) {
            Bitmap texture = loadImage(settings, s);
            assert (texture != null);
            int count = Integer.parseInt(settings.getString(s + "_count", "1"));

            float scalingFactor = Float.parseFloat(settings.getString(s + "_scalingFactor", "1"));

            float rotation = Float.parseFloat(settings.getString(s + "_rotation", "0"));

            for (int i = 0; i < count; i++) {
                Entity e = EntityFactory.fromBitmap(
                        texture,
                        rotation);
                e.x = r.nextInt(screenSizeX);
                e.y = r.nextInt(screenSizeY);
                entities.add(e);
            }
        }
    }

    public static void reset(SharedPreferences settings, Resources resources) {
        SharedPreferences.Editor newSettings = settings.edit();
        Set<String> deleteMe = settings.getStringSet("objects", new HashSet<String>());

        final String[] prefValues = resources.getStringArray(R.array.configuratorParametersName);
        //contains name of object in first position
        final String[] burgerOptions = resources.getStringArray(R.array.configuratorParameterStandardBurgerValue);
        final String[] pizzaOptions = resources.getStringArray(R.array.configuratorParameterStandardPizzaValue);

        //delete old preference
        for (String s : deleteMe) {
            for (String curr : prefValues) {
                newSettings.remove(s + "_" + curr);
            }
        }

        HashSet<String> set = new HashSet<>();
        set.add(burgerOptions[0]);
        set.add(pizzaOptions[0]);
        newSettings.putStringSet("objects", set);

        for (int i = 0; i < prefValues.length; i++) {
            newSettings.putString(burgerOptions[0] + "_" + prefValues[i], burgerOptions[i + 1]);
            newSettings.putString(pizzaOptions[0] + "_" + prefValues[i], pizzaOptions[i + 1]);
        }

        //make sure the images are correct
        newSettings.putString(burgerOptions[0] + "_" + "image", Integer.toString(R.drawable.burger));
        newSettings.putString(pizzaOptions[0] + "_" + "image", Integer.toString(R.drawable.pizza));

        //colors
        newSettings.putInt("bg_color_int", 0xFF3DAEE9);
        newSettings.apply();
    }

    private Bitmap loadImage(SharedPreferences settings, String s) {
        Bitmap texture;
        try {
            //load externalResource
            if (Boolean.parseBoolean(settings.getString(s + "_isExternalResource", "false"))) {
                texture = BitmapFactory.decodeFile(settings.getString(s + "_image", ""));
            } else {
                String id = settings.getString(s + "_image", null);
                texture = BitmapFactory.decodeResource(resources, Integer.parseInt(id));
            }
        } catch (Exception e) {
            //when failling to load texture, use the burger one
            texture = BitmapFactory.decodeResource(resources, R.drawable.burger);
            e.printStackTrace();
        }
        return texture;
    }
}
