/*
 * Licensed under GPL 3.0
 */

/*
 * Licensed under GPL 3.0
 */

package org.sasehash.burgerwp.View;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import org.sasehash.burgerwp.Model.Entity;
import org.sasehash.burgerwp.Model.WPState;

public class WPView {
    private WPState wpState;
    private Paint p;

    public WPView(WPState wpState, Paint p) {
        this.wpState = wpState;
        this.p = p;
    }

    public void draw(Canvas canvas) {
        for (Entity e : wpState.entities) {
            Matrix m = new Matrix();
            m.postRotate(e.rotation, e.height/2 , e.width/2);
            m.postScale(e.scale, e.scale);
            m.postTranslate(e.x, e.y);
            canvas.drawBitmap(e.texture, m, p);
        }
    }
}
